<?php
    require_once('animal.php');
    require_once('frog.php');
    require_once('ape.php');

    $hewan = new animal("Shaun");
    $hewan2 = new frog("Buduk");
    $hewan3 = new ape("Kera Sakti");

    echo "Name: " . $hewan->get_name() . "<br>";
    echo "Legs: " . $hewan->get_leg() . "<br>";
    echo "Cold Blood: " . $hewan->get_cold_blood() . "<br><br>";

    echo "Name: " . $hewan2->get_name() . "<br>";
    echo "Legs: " . $hewan2->get_leg() . "<br>";
    echo "Cold Blood: " . $hewan2->get_cold_blood() . "<br>";
    echo "Jump: " . $hewan2->Jump() . "<br><br>";

    echo "Name: " . $hewan3->get_name() . "<br>";
    echo "Legs: " . $hewan3->get_leg() . "<br>";
    echo "Cold Blood: " . $hewan3->get_cold_blood() . "<br>";
    echo "Jump: " . $hewan3->Yell() . "<br><br>";
?>