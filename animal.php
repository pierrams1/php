<?php
    class animal{
        private $name;
        private $leg = 4;
        private $cold_blood = "No";

        public function __construct($string){
            $this->name = $string;
        }

        public function set_leg($num){
            $this->leg = $num;
        }

        public function set_cold_blood($bool){
            $this->cold_blood = $bool;
        }

        public function get_name(){
            return $this->name;
        }

        public function get_leg(){
            return $this->leg;
        }

        public function get_cold_blood(){
            return $this->cold_blood;
        }
    }
?>
